package jp.alhinc.kubo_shinnosuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		try {
			Map<String, String> branchMap = new HashMap<String, String>();
			Map<String, Long> mapAggregate = new HashMap<>();
			BufferedReader br = null;
			try {
				File file = new File(args[0], "branch.lst");
				if (!file.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}
				br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null) {
					String[] shop = line.split(",");
					if (!shop[0].matches("[0-9]{3}") || (shop.length != 2)) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branchMap.put(shop[0], shop[1]);
					mapAggregate.put(shop[0], 0L);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}
			File[] list = new File(args[0]).listFiles();
			if (list != null) {
				FilenameFilter filter = new FilenameFilter() {
					public boolean accept(File file, String str) {
						if (str.matches("[0-9]{8}.rcd")) {
							return true;
						} else {
							return false;
						}
					}
				};
				try {
					File[] list1 = new File(args[0]).listFiles(filter);
					for (int i = 0; i < list1.length; i++) {
						Integer first = Integer.parseInt(list1[0].getName().substring(0, 8));
						Integer last = Integer.parseInt(list1[i].getName().substring(0, 8));
						if (last != first + i) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
						br = new BufferedReader(new FileReader(list1[i]));
						ArrayList<String> arraylist = new ArrayList<>();
						String line1;
						while ((line1 = br.readLine()) != null) {
							arraylist.add(line1);
						}
						if (!branchMap.containsKey(arraylist.get(0))) {
							System.out.println(list1[i].getName() + "の支店コードが不正です");
							return;
						}
						if (arraylist.size() != 2) {
							System.out.println(list1[i].getName() + "のフォーマットが不正です");
							return;
						}
						String sell = arraylist.get(0);
						long amount = Long.parseLong(arraylist.get(1));
						long totalSales = mapAggregate.get(sell) + amount;
						mapAggregate.put(sell, totalSales);
						if (!Long.toString(totalSales).matches("[0-9]{0,10}")) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
					}
				} finally {
					if (br != null) {
						br.close();
					}
				}
				BufferedWriter bw = null;
				try {
					File file1 = new File(args[0], "branch.out");
					bw = new BufferedWriter(new FileWriter(file1));
					for (Map.Entry<String, String> branchName : branchMap.entrySet()) {
						bw.write(branchName.getKey() + "," + branchName.getValue() + ","
								+ mapAggregate.get(branchName.getKey()));
						bw.newLine();
					}
				} finally {
					if (bw != null) {
						bw.close();
					}
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			e.printStackTrace();
			return;
		}
	}
}